export class Personagem {

    name: string;
    nickname: string;
    img: string;
    status: string;
    char_id: number;
    birthday: string;
    occupation: [];
    appearance: [];
    portrayed: string;

}
