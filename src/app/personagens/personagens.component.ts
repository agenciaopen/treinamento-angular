import { Component, OnInit } from '@angular/core';

import { ApiService } from '../services/api.service';
import { Personagem } from '../map/personagem';

@Component({
  selector: 'app-personagens',
  templateUrl: './personagens.component.html',
  styleUrls: ['./personagens.component.scss']
})
export class PersonagensComponent implements OnInit {

  personagens;

  constructor(public ws: ApiService) { }

  ngOnInit() {
    this.ws.GetAllCharacters().subscribe((response: any) => {
      this.personagens = response;
      // response.forEach(function (item, index) {
      //   console.log(item)
      // })
    })
  }

}