import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PersonagensComponent } from './personagens/personagens.component';
import { InternaPersonagemComponent } from './interna-personagem/interna-personagem.component';
import { ContatoComponent } from './contato/contato.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'personagens', component: PersonagensComponent },
  { path: 'interna-personagem/:id', component: InternaPersonagemComponent },
  { path: 'contato', component: ContatoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
