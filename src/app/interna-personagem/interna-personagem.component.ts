import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-interna-personagem',
  templateUrl: './interna-personagem.component.html',
  styleUrls: ['./interna-personagem.component.scss']
})
export class InternaPersonagemComponent implements OnInit {
  resposta = {
    name: "",
    img: "",
    nickname: "",
    occupation: [],
    birthday: "",
    portrayed: "",
    status: ""
  }
  constructor(public route: ActivatedRoute, public ws: ApiService) { }

  ngOnInit() {
    //console.log(this.route.snapshot.params.id)
    this.ws.GetSingleCharacter(this.route.snapshot.params.id).subscribe((response) => {
      this.resposta = response[0];
      console.log(this.resposta)
    })
  }

}
