import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternaPersonagemComponent } from './interna-personagem.component';

describe('InternaPersonagemComponent', () => {
  let component: InternaPersonagemComponent;
  let fixture: ComponentFixture<InternaPersonagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternaPersonagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternaPersonagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
