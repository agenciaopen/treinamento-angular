import { Component, OnInit, Input } from '@angular/core';
import { Personagem } from '../map/personagem';

@Component({
  selector: 'app-personagem',
  templateUrl: './personagem.component.html',
  styleUrls: ['./personagem.component.scss']
})
export class PersonagemComponent implements OnInit {

  @Input() nome: string;
  @Input() nick: string;
  @Input() status: string;
  @Input() imagem: string;
  @Input() id_char: number;


  constructor() { }

  ngOnInit() {

  }

}
