import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url: string = "https://www.breakingbadapi.com/api"
  constructor(public http: HttpClient) { }

  GetAllCharacters() {
    return this.http.get(`${this.url}/characters`)
  }

  GetSingleCharacter(id) {
    return this.http.get(`${this.url}/characters/${id}`)
  }
}
